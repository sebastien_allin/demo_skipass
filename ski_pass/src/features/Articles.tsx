import { createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";

type Item = {
  id: number;
  category: string;
};

const initialState: Item[] = [];

export const Articles = createSlice({
  name: "articles",
  initialState,
  reducers: {
    loadingState: (state, action: PayloadAction<Item>) => {
      const isAlready = state.find((state) => state.id === action.payload.id);

      !isAlready && state.push(action.payload);
    },
  },
});

export function getArticles() {
  return function (dispatch: Dispatch) {
    fetch("/api/api.json")
      .then((res) => res.json())
      .then((res) =>
        res.data.map((data: Item) => dispatch(loadingState(data)))
      );
  };
}

export const { loadingState } = Articles.actions;
export default Articles.reducer;
