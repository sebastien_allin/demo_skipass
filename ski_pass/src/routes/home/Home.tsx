import { Outlet } from "react-router-dom";
import Navebar from "../../components/Navebar";

export default function Home() {
  return (
    <>
      <Navebar />
      <div>
        <Outlet />
      </div>
    </>
  );
}
