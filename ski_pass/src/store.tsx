import { configureStore } from "@reduxjs/toolkit";
import articles from "./features/Articles";

export const store = configureStore({
  reducer: {
    articles,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
