export default function Navebar() {
  return (
    <div className="w-full h-40 bg-blue-950 flex items-center justify-center">
      <h1 className="text-4xl text-white">Ski Pass</h1>
    </div>
  );
}
