import { useDispatch, useSelector } from "react-redux";
import Image from "../assets/montagne.jpg";
import Card from "./Card";
import { AppDispatch, RootState } from "../store";
import { getArticles } from "../features/Articles";
import { useEffect } from "react";

export default function HomePage() {
  const articles = useSelector((state: RootState) => state.articles);
  console.log(articles.length);
  const dispatch: AppDispatch = useDispatch();

  if (articles.length === 0) {
    dispatch(getArticles());
  }

  return (
    <>
      <div className="w-full h-screen">
        <img src={Image} alt="" className="w-full relative" />
        <div className="absolute z-10 top-80 p-20 w-full flex justify-around flex-wrap ">
          {articles.map((article) => (
            <Card key={article.id} category={article.category} />
          ))}
        </div>
      </div>
    </>
  );
}
