type Props = {
  key: number;
  category: string;
};

export default function Card({ key, category }: Props) {
  return (
    <div key={key} className="w-80 h-80 bg-blue-50/50 text-stone-950 mb-20">
      <h2>{category} </h2>
    </div>
  );
}
