import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Home from "./routes/home/Home";
import HomePage from "./components/HomePage";
import Shop from "./routes/shop/Shop";

const router = createBrowserRouter([
  {
    element: <Home />,
    children: [
      { index: true, element: <HomePage /> },
      { path: "/shop/:shopId", element: <Shop /> },
    ],
  },
]);

function App() {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
